+++
title = "Welcome!"
description = "A welcome to the Veloren development blog"

date = 2030-01-01
weight = 0
+++

# Welcome to Veloren!

Veloren is an open-world, open-source multiplayer voxel RPG.

Veloren currently in an early stage of development, but you can already play the game.

We're pleased to say we've finally created a development blog (hint: you're reading it). On this blog, we'll regularly put up articles discussion various parts of Veloren's development, new additions, and a weekly *This week in Veloren*  update.

# Join Us

Veloren is open-source! Join us in making this game the best it can be.
* [Gitlab - Grab the source](https://gitlab.com/veloren/veloren)
* [Discord - Join the discussion](https://discord.gg/ecUxc9N)
* [Reddit - Get the latest updates to this blog at r/Veloren](https://www.reddit.com/r/Veloren/)

# Download

*Although we still support the 0.1.1 release of Veloren, development has since moved to the new engine.*

## Latest Stable Release, 0.1.0

**NOTICE: The public server for this release has been shut down. You can still play by running a local server, however.**

### Windows x64

[Download for Windows x64](https://gitlab.com/veloren/game/-/jobs/artifacts/v0.1.0/download?job=stable-windows-optimized)

To play the game, extract all files and run `voxygen.exe`, the 3D frontend.

If you want to host your own local server, run `server-cli.exe` in the background.

### Linux x64

[Download for Linux x64](https://gitlab.com/veloren/game/-/jobs/artifacts/v0.1.0/download?job=stable-linux-optimized)

To play the game, extract all files and run `voxygen`, the 3D frontend.

*Please Note: Voxygen currently has a command-line startup interface, so must be run from a terminal.*

If you want to host your own local server, run `server-cli` in the background.

### macOS

[Download for macOS](/download/macos.zip)

To run the game, extract all files and run `./voxygen`, the 3D frontend, from a terminal window.
Unfortunately due to a keyboard key mapping issue you cannot move around in this version.

*We do not currently provide a server build for macOS. If you wish to run a Veloren server on macOS, you can [compile it yourself](https://gitlab.com/veloren/game/wikis/Developer's-Corner/Introduction).*


## Latest Nightly Release, 0.1.1

**NOTICE: The public server for this release has moved to `veloren.mac94.de:38888`. This must be entered as a custom address when running the game.**

[Download for Windows x64](https://gitlab.com/veloren/game/-/jobs/artifacts/master/download?job=nightly-windows-debug)

To play the game, extract all files and run `voxygen.exe`, the 3D frontend.

If you want to host your own local server, run `server-cli.exe` in the background.

[Download for Linux x64](https://gitlab.com/veloren/game/-/jobs/artifacts/master/download?job=nightly-linux-debug)

To play the game, extract all files and run `voxygen`, the 3D frontend.

*Please Note: Voxygen currently has a command-line startup interface, so must be run from a terminal.*

If you want to host your own local server, run `server-cli` in the background.
